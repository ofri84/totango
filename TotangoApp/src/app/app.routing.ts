import { Routes, RouterModule } from '@angular/router';

import { BooksComponent } from './books/books.component';
import { AddBookComponent } from './books/add-book/add-book.component';
import { DetailsComponent } from './books/details/details.component';

const appRoutes: Routes = [
    {
        path: '',
        component: BooksComponent,
    },
    {
        path: 'add',
        component: AddBookComponent
    },
    {
        path: 'details/:isbn',
        component: DetailsComponent
    }
];

export const Routing = RouterModule.forRoot(appRoutes, { useHash: true });