import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service'
import { Location } from '@angular/common';
import swal from 'sweetalert2';
import * as moment from 'moment';

import { GenreMap } from '../../shared/helpers/genre';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss']
})
export class AddBookComponent implements OnInit {

  book = {};
  genreMap = [];
  selectedGenre = {};
  selectedDate = "";
  loading = false;

  datePickerConfig = { format: 'DD-MM-YYYY' }

  constructor(private api: ApiService, private location: Location) { }

  ngOnInit() {
    Object.keys(GenreMap).map(key => this.genreMap.push(GenreMap[key]));
  }

  save() {
    if (!this.book['title'] || !this.book['author'] || !this.book['isbn']) {
        swal("Please fill all required fields");
        return;
    }

    if (this.selectedGenre['key'])
        this.book['genre'] = this.selectedGenre['key'];
    if (this.selectedDate)
        this.book['publicationDate'] = moment(this.selectedDate, this.datePickerConfig.format).valueOf();

    this.loading = true;
    this.api.addBook(this.book).subscribe(res => {
      swal("Book " + this.book['title'] + " by " + this.book['author'] + " has been added");
      this.loading = false;
      this.goBack();
    }, error => {
      this.loading = false;
      swal({
        title: error.error,
        type: 'error'
      });
    })
  }

  goBack() {
    this.location.back();
  }

}
