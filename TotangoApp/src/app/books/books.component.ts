import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2'

import { GenreMap } from '../shared/helpers/genre';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  loading = false;  
  page = 0;
  total = 0;
  hasMore = false;
  books = [];
  genreMap = GenreMap;

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit() {
      this.fetchAllBooks();
  }

  fetchAllBooks() {
      this.loading = true;
      this.api.getAllBooks(this.page).subscribe(res => {
          this.books = res.books;
          this.hasMore = res.hasMore;
          this.total = res.total;
          this.loading = false;
      }, error => {
          console.warn("error", error);
          this.loading = false;
          swal({
            title: error.error,
            type: 'error'
          })
      });
  }

  selectBook(book) {
      this.router.navigate(['/details', book.isbn]);
  }

  addBook() {
      this.router.navigate(['/add']);
  }

  removeBook(book) {
      swal({
        title: 'Are you sure you want to delete ' + book.title + '?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it'
      }).then((result) => {
          if (result.value) {
              this.api.removeBook(book.isbn).subscribe(res => {
                this.books = this.books.filter(itBook => itBook['isbn'] !== book['isbn']);
              }, error => {
                swal({
                    title: error.error,
                    type: 'error'
                })
              });
          }
      })
  }

  pagination(action) {
    switch (action) {
        case 'next':
            this.page++;
            this.fetchAllBooks();
            break;
        case 'prev': 
            this.page--;
            this.fetchAllBooks();
            break;
    }
  }

}
