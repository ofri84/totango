import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import swal from 'sweetalert2'
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import * as moment from 'moment';
import { GenreMap } from '../../shared/helpers/genre';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  loading = false;
  book = {};
  bookLoaded = false;
  genreMap = GenreMap;

  constructor(
    private api: ApiService, 
    private router: Router,
    private route: ActivatedRoute,
    private location: Location) { }

  ngOnInit() {
    const isbn = parseInt(this.route.snapshot.paramMap.get('isbn'));
    if (isbn > 0)
      this.getBookDetails(isbn);
    else
      this.goBack();
  }

  getBookDetails(isbn) {
    this.loading = true;
    this.api.getBookDetails(isbn).subscribe(res => {
      this.book = res;
      if (res.publicationDate)
        this.book['publicationDate'] = moment(res.publicationDate).format("DD-MM-YYYY");
        
      this.bookLoaded = true;
      this.loading = false;
    }, error => {
      this.loading = false;

      swal({
        title: error.error,
        type: 'error'
      }).then(() => {
        this.goBack();
      });
    });
  }

  goBack() {
    this.location.back();
  }

}
