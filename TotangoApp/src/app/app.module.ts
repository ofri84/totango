import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Routing } from './app.routing';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { LoadingModule } from 'ngx-loading';
import { DpDatePickerModule } from 'ng2-date-picker';

import { ApiService } from './shared/services/api.service'

import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';
import { AddBookComponent } from './books/add-book/add-book.component';
import { DetailsComponent } from './books/details/details.component';

@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    AddBookComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    Routing,
    HttpModule,
    LoadingModule,
    DpDatePickerModule,
    MDBBootstrapModule.forRoot()  
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
