export const GenreMap = {
    'SCIENCE_FICTION': {val: 'Science Fiction', key: 'SCIENCE_FICTION'},
    'SATIRE': {val: 'Satire', key: 'SATIRE'}, 
    'DRAMA': {val: 'Drama', key: 'DRAMA'}, 
    'ACTION': {val: 'Action', key: 'ACTION'}, 
    'ROMANCE': {val: 'Romance', key: 'ROMANCE'}, 
    'MYSTERY': {val: 'Mystery', key: 'MYSTERY'}, 
    'HORROR': {val: 'Horror', key: 'HORROR'}
}