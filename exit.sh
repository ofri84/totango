#!/bin/bash -x
MONGOPID=`ps -ef | grep 'mongod' | grep -v grep | awk '{print $2}'`
echo "shutdown mongo service pid="$MONGOPID
kill $MONGOPID