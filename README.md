# Totango Assignment

Thank you for downloading the code.
this app use MongoDB so in order to run it you must have mongodb on your local machine.

## Setting MongoDB

if you don't have mongodb on your local machine you can download it using brew
`brew install mongodb` or see [Mongo docs](https://docs.mongodb.com/manual/administration/install-community/)

## Install 

on the terminal run `./install.sh` 

that will download all necessary dependencies and create the localdb directory. 

you can also do it manually by running `npm install` on each library (server & TotangoApp).

## Run app 

run `./start.sh` then navigate to `http://localhost:4200/`

## Close app

after pressing `Ctrl + c` run `./exit.sh`

that will close the mongod process which runs on the background

## Server Unit testing

for unit testing we use mocha.

downloading mocha to your local machine: `npm install mocha -g`

run unit tests: `cd server && mocha tests --recursive --watch`

## Further help

if anything went wrong in that process, please let me know:
ofri84@gmail.com, 054-4807609

Thanks, Ofri
