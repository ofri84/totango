const chai = require('chai');
const expect = chai.expect;
const mongoose = require('mongoose');

require('../models/Book');
const Book = mongoose.model('Book');

describe('NewBook', function() {
    const book = new Book({title: 'yes', author: 'haim', isbn: 123, genre: 'SATIRE'});
    it('validBook() should return false if input is not valid', function() {
        expect(book.validBook(book)).to.equal(true);
    });
    it('validGenre() should return false if genre is not valid', function() {
        expect(book.validGenre(book)).to.equal(true);
    });
});

