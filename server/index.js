const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const winston = require('winston');
const requestLogger = require('express-request-logs');

let isProduction = process.env.NODE_ENV === 'production'

const winstonLogger = new (winston.Logger)({ transports: [ new (winston.transports.Console)() ] });

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true })) // support encoded bodies

app.use(function(req, res, next){
  res.setHeader('Content-Type', 'application/json')
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Headers', '*')
  res.setHeader('Access-Control-Allow-Methods', '*')
  next()
});

app.use(requestLogger.create(winstonLogger, {
	plugins: {
    params: ({req, res, startTime}) => req.method === 'POST' ? 
      req.body : null
	},
	format: ({params, status, method, url, responseTime}) =>
		`HTTP ${method} ${url} status=${status} ${params ? 'body=' + JSON.stringify(params) : ''} took=${responseTime}`
}));

mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost:27017/totango', { useMongoClient: true });
//mongoose.set('debug', true);

//loads all models
require('./models/Book');

//loads all routes
app.use(require('./routes'));

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/// error handler
app.use(function(err, req, res, next) {

  if (!isProduction) 
    console.log(err.stack);

  res.status(err.status || 500);
  res.json({'error': err.message
  });
});

app.listen(3000, () => {
  console.log('listening on 3000')
});


