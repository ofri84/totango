const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const genreEnum = [
    'SCIENCE_FICTION', 
    'SATIRE', 
    'DRAMA', 
    'ACTION', 
    'ROMANCE', 
    'MYSTERY', 
    'HORROR'
];

const BookSchema = new mongoose.Schema({
    title: {type: String, required: [true, "can not be blank"]},
    description: String,
    isbn: {type: Number, unique: true, required: [true, "can not be null"]},
    author: {type: String, required: [true, "can not be blank"]},
    publicationDate: Number,
    price: Number,
    genre: {
        type: String,
        enum: genreEnum
    }
}, {timestamps: true})

BookSchema.plugin(uniqueValidator, {message: 'is already used.'})


BookSchema.methods.validBook = function(book) {
    return book.title !== undefined 
        && book.author !== undefined 
        && book.isbn !== undefined;
}

BookSchema.methods.validGenre = function(book) {
    if (book.genre === undefined) return true;
    return genreEnum.indexOf(book.genre) !== -1;
}

mongoose.model('Book', BookSchema);