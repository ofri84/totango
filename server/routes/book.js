const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const Book = mongoose.model('Book')

router.get('/all', function(req, res, next) {
    /** 
     * pagination, by default page is 0 and limit is 15
    */
    const page = parseInt(req.query.page) || 0;
    const limit = parseInt(req.query.limit) || 15;

    let countPromise = Book.count();
    let booksPromise = Book.find()
        .skip(page * limit)
        .limit(limit)
        .select('title author isbn price genre');

    Promise.all([countPromise, booksPromise]).then(function(values) {
        const hasMore = values[0] > (page + 1) * limit;
        return res.send({total: values[0], hasMore: hasMore, books: values[1]});
    }).catch((error) => {
        return res.status(500).send(error);
    });
})

router.get('/', function(req, res, next) {
    const bookIsbn = req.query.isbn;
    if (typeof bookIsbn === 'undefined')
        return res.status(400).send({error: "missing isbn number"})
    
    Book.findOne({isbn: bookIsbn}).then(function(book) {
        if (!book) 
            return res.status(400).send({error: "invalid isbn number"}) 

        return res.send(book)
    }).catch(next);
})

router.post('/', function(req, res, next) {
    const newBook = new Book(req.body);

    if (!newBook.validBook(newBook) || !newBook.validGenre(newBook))
        return res.status(400).send({error: 'non valid input fields'});

    newBook.save().then(function(book) {
        return res.status(201).send(newBook);
    }).catch(next);
})

router.delete('/', function(req, res, next) {
    const bookIsbn = req.query.isbn;
    if (typeof bookIsbn === 'undefined')
        return res.status(400).send({error: "missing isbn number"})

    Book.findOneAndRemove({isbn: bookIsbn}).then(function(book) {
        if (!book) 
            return res.status(400).send({error: "invalid isbn number"})
        
        return res.send({result: "success"})
    }).catch(next);
})

module.exports = router