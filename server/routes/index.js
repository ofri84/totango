const express = require('express')
const router = express.Router()

router.use('/book', require('./book'))

router.get('/', function(req, res) {
    res.render('index')
})

module.exports = router